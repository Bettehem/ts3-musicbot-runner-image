FROM docker.io/library/archlinux:base

LABEL org.opencontainers.image.vendor="Tobias Wiese"
LABEL org.opencontainers.image.authors="git@twiese99.de"
LABEL org.opencontainers.image.version="1.0.0"
LABEL org.opencontainers.image.url="https://gitlab.com/twiese99/ts3-musicbot-runner-image"
LABEL org.opencontainers.image.source="https://gitlab.com/twiese99/ts3-musicbot-runner-image"

ENV JAVA_HOME=/usr/lib/jvm/default
ENV GRADLE_HOME=/usr/share/java/gradle

COPY install.sh /tmp/
RUN chmod +x /tmp/install.sh && bash /tmp/install.sh
