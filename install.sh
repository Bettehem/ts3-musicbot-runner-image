#!/bin/bash

pacman -Syu pacman-contrib gradle kotlin jdk11-openjdk java11-openjfx yt-dlp git base-devel --noconfirm --needed
paccache -rk0
archlinux-java set java-11-openjdk
mkdir -p /home/build
chgrp nobody /home/build
chmod g+ws /home/build
setfacl -m u::rwx,g::rwx /home/build
setfacl -d --set u::rwx,g::rwx,o::- /home/build
su -c "cd /home/build; git clone https://aur.archlinux.org/yt-dlp-drop-in.git; cd yt-dlp-drop-in; makepkg" - nobody
pacman -U /home/nobody/yt-dlp-drop-in/*.zst --noconfirm
rm -rf /home/nobody/yt-dlp-drop-in
